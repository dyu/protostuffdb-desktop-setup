Run this from your terminal:
sudo ./setup

Installation will be in /opt which requires priviliges.
Alternatively, you can run without sudo and simply follow the instructions after running the executable.

Wait for the extraction to finish.  The installation will be corrupted if you interrupt it.
