#!/bin/sh

mkdir -p dist/linux64 dist/win64

cd win64
#7za a -t7z ../dist/win64/setup.7z .
zip -r ../dist/win64/setup.zip .

cd ../linux64
#tar -cjf ../dist/linux64/setup.tar.xz .
tar -czf ../dist/linux64/setup.tar.gz .

